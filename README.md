## Encryption structure

We use Aes Managed Encryption Algorithm with Ciphermode ECB. The data that we encrypt is a JSON-string with structure from the "RequestModel.cs" file. 

The output from our encryption engine generates an IV-KEY and ENCRYPTION-DATA.

IV-KEY	 		: Public key needed on both ends to encrypt/decryot data.
ENCRYPTION-DATA : JSON data encrypted.

To be able to encrypt and decrypt data we also need to provide the AES-KEY located in "BravoEncryptionEngine.cs". This key is a private key that needs to be store in a safe environment and provided to all instances that will encrypt/decrypt data.

We strongly suggest that the AES-KEY are environment-unique (DEV/TEST/PROD etc...).

---

## Outgoing Request

The website "www.flygbra.se" will post encrypted user data to a provided bank-URL of choice using FormData. The FormData will include the following fields:


# TIME
Name: TIME

Value: [Encryption date, format: yyyyMMddHHmmss]

Type: string


# IV

Name: IV

Value: [IV-Key string]

Type: string


# DATA

Name: DATA

Value: [Encrypted json string]

Type: string