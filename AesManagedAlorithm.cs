﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;


namespace CapGeminiConsidBravoSnippets
{
    public class AesManagedAlgorithm
    {
        public static byte[] EncryptStringToBytes(string plainText, byte[] aesKey, byte[] iV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("Plaintext to be encrypted contains no elements");
            }
            if (aesKey == null || aesKey.Length <= 0)
            {
                throw new ArgumentNullException("The given key is either null or missing");
            }

            var aesAlgorithm = new AesManaged
            {
                KeySize = 128,
                Key = aesKey,
                BlockSize = 128,
                Mode = CipherMode.ECB,
                IV = iV
            };

            //Need to package data in a base64 string
            var base64String = Convert.ToBase64String(plainText.ConvertToByteArray());

            ICryptoTransform encryptor = aesAlgorithm.CreateEncryptor(aesAlgorithm.Key, aesAlgorithm.IV);
            var encryptedByteArray = encryptor.TransformFinalBlock(base64String.ConvertToByteArray(), 0, base64String.Length);
            return encryptedByteArray;
        }

        public static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] aesKey, byte[] iV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (aesKey == null || aesKey.Length <= 0)
                throw new ArgumentNullException("aesKey");
            if (iV == null || iV.Length <= 0)
                throw new ArgumentNullException("iV");

            // Declare the string used to hold
            // the decrypted text.
            string base64String = string.Empty;

            // Create an AesCryptoServiceProvider object
            // with the specified key and iV.
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = aesKey;
                aesAlg.IV = iV;
                aesAlg.Mode = CipherMode.ECB;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            base64String = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            byte[] base64Data = Convert.FromBase64String(base64String);
            return Encoding.UTF8.GetString(base64Data);
        }
    }
}