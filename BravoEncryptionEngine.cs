﻿using System;
using System.Security.Cryptography;

namespace CapGeminiConsidBravoSnippets
{
    public class BravoEncryptionEngine
    {
        //Shared secret key for encrypt/decrypt data. Store in safe environment.
        private static readonly string SECRET_SAFE_KEY = "27323A0825226DDD316881852610DACB81210355C3117DAD83EF5EE9E8602915";

        //AesKey as bytearray used for encryption/decryption
        private static byte[] AesKeyArray = SECRET_SAFE_KEY.HexStringToByteArray();

        public static (string IVString, string EncryptedString) Encrypt(string str)
        {
            var ivString = "";
            var encryptedString = "";

            try
            {
                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.Mode = CipherMode.ECB;
                    ivString = aesAlg.IV.ByteArrayToString();
                    encryptedString = AesManagedAlgorithm.EncryptStringToBytes(str, AesKeyArray, aesAlg.IV).ByteArrayToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return (ivString, encryptedString);
        }

        public static string DecryptString(string ivString, string encryptedString)
        {
            return AesManagedAlgorithm.DecryptStringFromBytes_Aes(encryptedString.HexStringToByteArray(), AesKeyArray, ivString.HexStringToByteArray());
        }
    }
}
