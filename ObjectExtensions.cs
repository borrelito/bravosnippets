﻿using System;
using System.Globalization;
using System.Text;

namespace CapGeminiConsidBravoSnippets
{
    public static class ObjectExtensions
    {
        public static byte[] HexStringToByteArray(this string hexString)
        {
            if (hexString.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "The binary key cannot have an odd number of digits: {0}", hexString));
            }

            byte[] HexAsBytes = new byte[hexString.Length / 2];
            for (int index = 0; index < HexAsBytes.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                HexAsBytes[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return HexAsBytes;
        }

        public static string ByteArrayToString(this byte[] ba)
        {
            var hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }

        public static byte[] ConvertToByteArray(this object o)
        {
            if (o == null || String.IsNullOrEmpty(o.ToString()))
            {
                return null;
            }

            var encoding = new UTF8Encoding();
            return encoding.GetBytes(o.ConvertToString());
        }

        public static string ConvertToString(this object o)
        {
            if (o == null || o == DBNull.Value)
                return null;
            return o.ToString();
        }
    }
}
