﻿namespace CapGeminiConsidBravoSnippets
{
    /// <summary>
    /// Model used to output JSON data
    /// </summary>
    public class RequestModel
    {
        /// <summary>
        /// FrequentFlyerId
        /// </summary>
        public string FrequentFlyerId { get; set; }

        /// <summary>
        /// FirstName
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// SurName
        /// </summary>
        public string SurName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// BirthDate = format: 20180530
        /// </summary>
        public string BirthDate { get; set; }

        /// <summary>
        /// PhoneNumber
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gender - M or F
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// AdressField1
        /// </summary>
        public string AdressField1 { get; set; }

        /// <summary>
        /// AdressField2
        /// </summary>
        public string AdressField2 { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Zip
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// TierLevel
        /// </summary>
        public string TierLevel { get; set; }

        /// <summary>
        /// ExpiryDate = format: 20180530
        /// </summary>
        public string ExpiryDate { get; set; }

        /// <summary>
        /// ReturnUrl
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}
