﻿using System;
using Newtonsoft.Json;

namespace CapGeminiConsidBravoSnippets
{
    class Program
    {
        static void Main(string[] args)
        {
            //Data to send in request
            var requestModel = new RequestModel
            {
                FrequentFlyerId = "123",
                FirstName = "Daniel",
                SurName = "Söderberg",
                Email = "daniel@flyg.bra",
                BirthDate = "19850323",
                PhoneNumber = "0046701234567",
                Gender = "M",
                AdressField1 = "Street 1",
                AdressField2 = "PO Box 343",
                City = "Heaven",
                Zip = "54321",
                TierLevel = "[TierLevel]",
                ExpiryDate = "20190910",
                ReturnUrl = "https://www.flygbra.se"
            };

            //Create JSON string from RequestModel
            var jsonData = JsonConvert.SerializeObject(requestModel);

            Console.WriteLine("### DATA TO ENCRYPT");
            Console.WriteLine();
            Console.WriteLine(jsonData);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

            //Encrypt JSON - returns IV and Encrypted JSON strin
            var (iv, encryptedData) = BravoEncryptionEngine.Encrypt(jsonData);
            
            //Encryption result
            Console.WriteLine("### ENCRYPTION RESULT");
            Console.WriteLine();
            Console.WriteLine("IV: {0}", iv);
            Console.WriteLine("DATA: {0}", encryptedData);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();

            //Decryption result
            var decryptionString = BravoEncryptionEngine.DecryptString(iv, encryptedData);
            Console.WriteLine("### DECRYPTION RESULT");
            Console.WriteLine();
            Console.WriteLine(decryptionString);

            while (true) { }
        }
    }
}
